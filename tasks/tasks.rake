# references
# http://tech.natemurray.com/2007/03/ruby-shell-commands.html

task :hola do
    puts "Hola..."  
end

task :install do
    puts `npm install -d`
end

task :start do
    exec 'DEBUG=myapp npm start'
    # exec 'mongo admin --eval 'db.shutdownServer()' > db/logs'
end

task :mongod do
	exec 'mongod --dbpath db/data --fork --logpath db/logs/log'
	# exec 'mongod --dbpath ~/.mongodb/data/db/'
end
